require 'rails_helper'

RSpec.describe 'Products', type: :system do
  describe 'GET #Show' do
    include ApplicationHelper
    let!(:product) { create(:product) }

    before do
      visit potepan_product_path(product.id)
    end

    it "ruby language is working on show page" do
      expect(page).to have_title full_title(page_title: product.name)

      expect(page).to have_selector 'h2', text: product.name
      expect(page).to have_selector 'li', text: product.name
      expect(page).to have_selector 'p',  text: product.description
      expect(page).to have_selector 'h3', text: product.display_price

      expect(page).to have_link 'logo_test', href: potepan_index_url
      click_link "logo_test"
      expect(page).to have_selector 'title', text: full_title
      visit potepan_product_path(product.id)

      # There are two Home_link in show.html.erb
      expect(page).to have_link 'Home', href: potepan_index_url
      find(".Home_link_test_in_NAVBAR").click
      expect(page).to have_selector 'title', text: full_title(page_title: '')
      visit potepan_product_path(product.id)

      find(".Home_link_test_in_Light_Section").click
      expect(page).to have_selector 'title', text: full_title(page_title: nil)
      visit potepan_product_path(product.id)

      expect(page).to have_selector 'p', text: "2016-#{Time.current.year}"
    end
  end
end
