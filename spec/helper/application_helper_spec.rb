require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper

  describe '#full_title method' do
    let!(:product) { create(:product) }

    context 'when not passing argument' do
      it "retun only bas_title" do
        expect(full_title).to eq(SERVICE_NAME)
      end
    end

    context 'when argument is nil' do
      it 'return page_title plus base_title' do
        expect(full_title(page_title: nil)).to eq(SERVICE_NAME)
      end
    end

    context 'when argument is space' do
      it 'return page_title plus base_title' do
        expect(full_title(page_title: ' ')).to eq(SERVICE_NAME)
      end
    end

    context 'when argument is exist' do
      it 'return page_title plus base_title' do
        expect(full_title(page_title: product.name)).to eq("#{product.name} - #{SERVICE_NAME}")
      end
    end
  end
end
