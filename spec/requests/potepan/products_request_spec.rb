require 'rails_helper'

RSpec.describe 'Products', type: :request do
  describe 'Request GET#Show' do
    let!(:product) { create(:product) }

    before { get potepan_product_path(product.id) }

    it 'returns 200 response' do
      expect(response.status).to eq 200
    end

    it 'response success' do
      expect(response).to be_success
    end

    context 'user not found' do
      subject { -> { get potepan_product_url product.id + 1 } }

      it { is_expected.to raise_error ActiveRecord::RecordNotFound }
    end
  end
end
