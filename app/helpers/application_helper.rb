module ApplicationHelper
  def full_title(page_title: '')
    if page_title.blank?
      SERVICE_NAME
    else
      "#{page_title} - #{SERVICE_NAME}"
    end
  end
end
